# Dictionary - Java based application

Dark Theme | Light Theme
:---------:|:---------:
![Image of Dictionary](https://gitlab.com/gaborkolozsy/Dictionary/raw/master/resources/images/Dictionary-Dark.png) | ![Image of Dictionary](https://gitlab.com/gaborkolozsy/Dictionary/raw/master/resources/images/Dictionary-Light.png)
<br>

# Description

This `Dictionary` is an offline dictionary. It includes all combination of the following
3 languages: english, german and hungarian.  
You can search for over `70.000` words, combination of words and sentences per dictionary. :smile:
<br>
<br>
# Dictionaries

|Dictionaries                   | Count of Words |
|:-----------------------------:|:--------------:|
| :gb: `English-Hungarian` :hu: | **169425**     |
| :hu: `Hungarian-English` :gb: | **91688**      |
| :de: `German-Hungarian`  :hu: | **129481**     |
| :hu: `Hungarian-German`  :de: | **69995**      |
| :gb: `English-German`    :de: | **141287**     |
| :de: `German-English`    :gb: | **193860**     |